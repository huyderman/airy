# Airy - OpenAI Powered CLI Tool for Editing Text or Code

Airy is a CLI tool for editing text documents and code files with the power of
[OpenAI](https://beta.openai.com/) models.

It lets you, for instance, fix spelling mistakes, insert missing `import`
statements in your code, enhance your text documents by adding details, and much
more -- using OpenAI models.

It provides a command-line interface (CLI) for using the latest state-of-the-art
models of OpenAI, currently available in beta. This allows you to edit your text
documents and code files with all the power of machine learning, without
understanding the technical details.

The code is written in [Ruby](https://www.ruby-lang.org/). I have used the
package manager [Bundler](https://bundler.io/) to distribute the tool as a
Rubygem. This is currently not in RubyGems.org, so you will have to build
locally.

It is for demonstration purposes only; it currently has a lot of rough edges, it
is not yet fully tested, and it needs improving to be really useful in a
production setting.

*Note: This README was written using Airy (with some manual editing and multiple
revision rounds).*

## Features

- **All the power of OpenAI** – Edit your text documents and code files with the
  power of [OpenAI](https://beta.openai.com/) models.
- **Easy to use** – No need to understand the details of the OpenAI API.
- **Various options** – Airy lets you fine-tune the editing with different
  options.

## Installation

The project is hosted at [GitLab](https://gitlab.com/huyderman/airy). Clone the
repo:

```sh
$ git clone https://gitlab.com/huyderman/airy.git
```

With Ruby installed on your computer, you can install the required dependencies
by running the following commands:

```sh
$ cd airy
$ bundle install
```

## Configuration

First, you need to get an API key on the
[OpenAI API website](https://beta.openai.com/).

Create `config.yml` and set OpenAI access token:

```yaml
---
openai:
  access_token: <TOKEN>
```

## Getting Started

Create `hello.txt` with your favourite text editor, e.g.:

```text
hello, world!
```

Then, edit `hello.txt` with Airy:

```sh
$ airy hello.txt
```

Airy will first prompt for your **edit instructions**:

```text
Edit instructions: (Fix the spelling mistakes)
```

Airy will then present its suggestion in an external merge-tool (default is
currently [`meld`](http://meldmerge.org/), but see below). You can then review
and either confirm or reject the suggested changes.

## Usage

You can edit code files with the code model:

```sh
$ airy --code hello.py
```

and text files with the default text model:

```sh
$ airy hello.txt
```

The default mergetool is `meld`; override with `--tool` option:

```sh
$ airy --tool=vimdiff hello.txt
```

Use `--help` for more options.

```sh
$ airy --help
Input file required


Usage: airy [OPTIONS] INPUT-FILE

Options:
  -3, --3way                Do a 3-way merge instead of 2-way
  -c, --code                Use model optimized for code
  -d, --debug               Print debug information
  -h, --help                Print usage
  -I, --instruction=string
      --no-confirm          Don't ask for confirmation
      --temperature (default 0.9)
  -t, --tool=string         Mergetool to use for result (default "meld")
```

## Note

Airy uses the OpenAI edit model currently in beta. It is free while in beta, but
this may change in the future.

## License

The gem is available as open source under the terms of the
[MIT License](https://opensource.org/licenses/MIT).
