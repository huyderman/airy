# frozen_string_literal: true

module Airy
  module Utils
    module_function

    # Estimates the token count for a given string.
    # @note This is a rough estimate, based on the ~4 characters per token.
    #       For `code` this might be an overestimate as it handles whitespace more efficiently,
    #       but is likely an underestimate for `text`.
    # @param text [String] the string to estimate the token count for
    # @return [Range]
    def estimate_token_count(text)
      Range.new((text.length / 5.0).ceil, (text.length / 4.0).ceil)
    end
  end
end
