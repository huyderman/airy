# frozen_string_literal: true

require 'xdg'
require 'tty-config'

module Airy
  APP_NAME = 'airy'
  module Config
    XDG = XDG::Environment.new
    module OpenAI
      module_function

      def access_token
        Config.config.fetch(:openai, :access_token)
      end

      def access_token=(token)
        Config.config.set(:openai, :access_token, value: token)
      end
    end

    module_function

    def config
      return @config if @config

      @config = TTY::Config.new
      @config.env_prefix = APP_NAME
      @config.autoload_env

      @config.append_path File.join(Config::XDG.config_home, APP_NAME)
      Config::XDG.config_dirs.each do |dir|
        @config.append_path File.join(dir, APP_NAME)
      end
      @config.append_path File.expand_path('../..', __dir__)
      @config.read if @config.exist?

      @config
    end

    def save(force: false)
      config.write(create: true, force: force)
    end
  end
end
