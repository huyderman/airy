# frozen_string_literal: true

require 'airy/config'
require 'airy/tools'
require 'airy/utils'
require 'ruby/openai'
require 'tty-file'
require 'tty-link'
require 'tty-option'
require 'tty-prompt'
require 'tty-spinner'

module Airy
  module CLI
    class Edit
      include TTY::Option
      include Airy::Utils

      def initialize
        super
        @prompt = TTY::Prompt.new
        @output_changed = false
      end

      usage do
        program 'airy'
        no_command
      end

      argument :input_file do
        required
        convert :path
      end

      option :instruction do
        short '-I'
        long '--instruction=string'
        optional
      end

      option :temperature do
        convert :float
        default 0.9
      end

      # option :n do
      #   convert :int
      #   default 1
      # end

      option :tool do
        short '-t'
        long '--tool=string'
        default 'auto'
        desc 'Mergetool to use for result'
      end

      flag :code do
        short '-c'
        long '--code'
        desc 'Use model optimized for code'
      end
      def codex?
        params[:code] ? true : false
      end

      def model
        if codex?
          'code-davinci-edit-001'
        else
          'text-davinci-edit-001'
        end
      end

      flag :no_confirm do
        desc 'Don\'t ask for confirmation'
      end

      def no_confirm?
        params[:no_confirm]
      end

      flag :help do
        short '-h'
        long '--help'
        desc 'Print usage'
      end

      env :debug do
        desc 'Print debug information'
        convert :bool
        default false
      end

      flag :version do
        long '--version'
        desc 'Print the version number, then exit'
      end


      # Config ENV variables
      env Config.config.to_env_key("openai.access_token") do
        desc 'OpenAI access token'
      end
      env Config.config.to_env_key("tool.cmd") do
        desc 'Default difftool command'
      end
      env Config.config.to_env_key("tool.path") do
        desc 'Path to difftool'
      end

      def run
        show_help_and_exit if params[:help]
        show_version_and_exit if params[:version]
        init

        if no_confirm?
          edit_file
        else
          run_edit_loop
        end

        cleanup
      end

      private

      def show_version_and_exit
        show "airy #{Airy::VERSION}"
        exit
      end

      def show_help_and_exit
        show help
        exit
      end

      def run_edit_loop
        loop do
          edit_file

          case show_repeat_edit_prompt
          when :yes then next
          when :no then break
          when :edit then @instruction = ask_instructions
          when :abort then revert_input_file and break
          else error('Unknown choice') and break
          end
        end
      end

      def show_repeat_edit_prompt
        question = if @output_changed
                     'Repeat edit?'
                   else
                     'File unchanged, try again?'
                   end
        @prompt.expand(question, auto_hint: true) do |q|
          q.default @output_changed ? 2 : 1
          q.choice key: 'y', name: 'Yes', value: :yes
          q.choice key: 'n', name: 'No', value: :no
          q.choice key: 'e', name: 'Edit Instruction', value: :edit
          q.choice key: 'b', name: 'Abort', value: :abort
        end
      end

      def revert_input_file
        TTY::File.copy_file(@original_file, @input_file, force: true)
      end

      def ask_instructions
        ask 'Edit instructions:', value: @instruction || '',
                                  default: 'Fix the spelling mistakes'
      end

      def init
        Tools::DiffTool.init(tool: params[:tool])
        @client = create_openai_client(fetch_access_token)

        @input_file = params[:input_file]
        verify_input_file

        @output_file = @input_file
        @edit_file = create_filename(@input_file, 'edit')
        @original_file = create_filename(@input_file, 'original')

        @instruction = params[:instruction]
        @instruction ||= ask_instructions

        backup_input_file
      end

      def create_filename(filename, ext)
        dirname = File.dirname(filename)
        extname = File.extname(filename)
        basename = File.basename(filename, extname)
        File.expand_path("#{basename}.#{ext}#{extname}", dirname)
      end

      def cleanup
        TTY::File.remove_file(@original_file, force: no_confirm?)
      end

      def verify_input_file
        if @input_file
          return if @input_file.file?

          show_error_and_exit(<<~ERROR, show_help: true)
            Input file #{@input_file} not found or a directory

          ERROR
        end

        show_error_and_exit(<<~ERROR, show_help: true)
          Input file required

        ERROR
      end

      def backup_input_file
        TTY::File.copy_file(@input_file, @original_file, force: no_confirm?)
        @original_checksum = checksum_file(@original_file)
      end

      MAX_TOKENS = 3000

      def edit_file
        # TODO: Add error handling for OpenAI requests and merge
        # TODO: support writing output to a different file
        input = @input_file.read

        validate_token_count!(input)

        spinner = TTY::Spinner.new('[:spinner] Sending request to OpenAI...')
        spinner.run('Done!') do
          result = create_edit(@instruction, input)
          TTY::File.create_file(@edit_file, result, force: no_confirm?)
        end

        Tools::DiffTool.run(edit: @edit_file, target: @input_file,
                            original: @original_file)
        @output_changed ||= checksum_file(@output_file) != @original_checksum
        TTY::File.remove_file(@edit_file, force: no_confirm?)
      end

      def validate_token_count!(input)
        token_count = estimate_token_count(input + @instruction)
        safe, probably_safe = token_count.minmax.map do |tokens|
          tokens < MAX_TOKENS
        end

        return true if safe

        if no_confirm?
          return true if probably_safe

          show_error_and_exit(<<~ERROR, show_help: false)
            Number of tokens exceeds number of allowed tokens.
            Estimated number of tokens are #{token_count}, which is higher than #{MAX_TOKENS}
          ERROR
        end

        exit unless @prompt.yes?(<<~TEXT.chomp, default: probably_safe)
          Number of tokens (est. #{token_count}) might be higher than #{MAX_TOKENS}, continue?
        TEXT
      end

      def checksum_file(file)
        TTY::File.checksum_file(file, 'sha1')
      end

      def create_edit(instruction, input)
        response = @client.edits(
          parameters: {
            model: model,
            input: input,
            instruction: instruction,
            temperature: params[:temperature]
            # n: params[:n]
          }
        )

        error = response['error']

        if error
          show_error_and_exit(<<~ERROR, show_help: false)
            [ERROR]: #{error['message']}
          ERROR
        end

        if params[:debug]
          usage = response['usage']
          show <<~LOG
            [DEBUG] Token Usage:
              Prompt: #{usage['prompt_tokens']}
              Completion: #{usage['completion_tokens']}
              Total: #{usage['total_tokens']}
          LOG
        end

        response.dig('choices', 0, 'text')
      end

      def create_openai_client(access_token)
        OpenAI::Client.new(access_token: access_token)
      end

      def fetch_access_token
        access_token = Config::OpenAI.access_token
        unless access_token
          error <<~TEXT
            An OpenAI access token is required for accessing the API.
            You must either set it in your config file, or through
            the Environmental variable `AIRY_OPENAI_ACCESS_TOKEN`.
          TEXT
          exit if no_confirm?

          if @prompt.yes?("Set token now, creating a new config file if missing?")
            link = TTY::Link.link_to('OpenAI API Account page', 'https://platform.openai.com/account/api-keys')
            access_token = ask(<<~TEXT.chomp)
              You can create an API key on the #{link}
              Token:
            TEXT
            Config::OpenAI.access_token = access_token
            Config.save(force: true)
          else
            exit
          end
        end
        access_token
      end

      def show(message, **opts)
        @prompt.say(message, **opts)
      end

      def error(*args)
        @prompt.error(*args)
      end

      def ask(message = '', **options, &block)
        @prompt.ask(message, **options, &block)
      end

      def show_error_and_exit(message, show_help: false)
        error message
        show help if show_help
        exit
      end
    end
  end
end
