# frozen_string_literal: true

require 'tty-command'
require 'tty-platform'
require 'tty-prompt'
require 'tty-which'
require 'airy/config'

module Airy
  module Tools
    module Internal
      class DiffTool
        include Airy::Config
        def initialize
          @platform = TTY::Platform.new
          @prompt = TTY::Prompt.new
          @cmd = TTY::Command.new(printer: :quiet)
        end

        def init(tool: 'auto')
          @tool_cmd = tool
          if @tool_cmd == 'auto'
            @tool_cmd = config.fetch(:tool,
                                     :cmd, default: nil)
          end
          unless @tool_cmd
            @prompt.warn <<~TEXT.chomp
              Mergetool not given as argument or in config, falling back to best guess
            TEXT
            @tool_cmd = guess_tool
            @prompt.warn "Found `#{@tool_cmd}`"
          end
          @tool_path = config.fetch(:tool, :path, default: nil)
        end

        def run(edit:, target:, original:)
          # TODO: Resolve with @tool_path if set
          # TODO: Add support for outputs that are different from original, e.g:
          # `meld $ORIGINAL $EDIT --output=$TARGET`

          cmd, *args = tool_path.split

          case cmd
          in 'smerge'
            @cmd.run(cmd, 'mergetool', target, edit)
          in /^jetbrains/ | /clion/ | /datagrip/ | /goland/ | /idea/ | /phpstorm/ | /pycharm/ | /rider/ | /rubymine/ | /webstorm/
            @cmd.run(cmd, 'diff', target, edit, only_output_on_error: true)
          in /^vim/
            system("#{cmd} #{target} #{edit}")
          in 'emacs' | 'emerge'
            @cmd.run('emacs', '-f', 'emerge-files-command', target,
                     edit, pty: true)
          else
            @cmd.run(cmd, *args, target, edit)
          end
        end

        private

        def tool_path
          File.join(*[@tool_path, @tool_cmd].compact)
        end

        def guess_tool
          tool_candidates.find do |tool|
            TTY::Which.exist? tool
          end
        end

        def tool_candidates
          graphical_candidates + tty_candidates
        end

        def graphical_candidates
          desktop = ENV.fetch('XDG_CURRENT_DESKTOP', nil)
          return [] unless desktop

          tools = []
          tools += %w[kompare]
          tools += if desktop == 'GNOME'
                     %w[meld opendiff kdiff3 tkdiff]
                   else
                     %w[opendiff kdiff3 tkdiff meld]
                   end
          tools += %w[gvimdiff diffuse diffmerge p4merge araxis bc codecompare]
          tools += %w[smerge]
          tools
        end

        def tty_candidates
          case ENV.fetch('VISUAL', nil) || ENV.fetch('EDITOR', nil)
          in /nvim/
            %w[nvimdiff vimdiff emacs]
          in /vim/
            %w[vimdiff nvimdiff emacs]
          else
            %w[emacs vimdiff nvimdiff]
          end
        end
      end
    end

    DiffTool = Internal::DiffTool.new
  end
end
