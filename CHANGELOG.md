# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

Initial version of Airy

### Added

- Support for submitting a file with an edit prompt to the OpenAI API using the
  `edit` models.
- Launching a difftool/mergetool to merge the response to the submitted file
- A simple life-cycle loop for doing multiple sequential edit prompts 
- Basic "best-guess" resolution of difftool/mergetool and some basic configs
- Direct user towards creating OpenAI token if missing, and create config file

[unreleased]: https://gitlab.com/huyderman/airy/-/commits/main
