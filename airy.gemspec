# frozen_string_literal: true

require_relative('lib/airy/version')

Gem::Specification.new do |s|
  s.name = 'airy'
  s.version = Airy::VERSION
  s.summary = 'Airy is a CLI tool for editing text documents and code files with the power of OpenAI models.'
  s.authors = ['Jo-Herman Haugholt']
  s.email = ['johannes@huyderman.com']
  s.homepage = 'https://gitlab.com/huyderman/airy'
  s.metadata = {
    'bug_tracker_uri' => "#{s.homepage}/-/issues",
    'changelog_uri' => "#{s.homepage}/-/blob/main/CHANGELOG.md",
    'documentation_uri' => 'https://www.rubydoc.info/gems/airy',
    'homepage_uri' => s.homepage,
    'source_code_uri' => s.homepage,
    'rubygems_mfa_required' => 'true'
  }
  s.license = 'MIT'

  s.files = Dir['lib/**/*']
  s.files += Dir['bin/*']
  s.files += Dir['[A-Z]*'].reject { |file| /\.lock$/.match(file) }
  s.executables = ['airy']
  s.require_paths = ['lib']

  s.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE']

  s.required_ruby_version = '>= 3.0.0'

  s.add_runtime_dependency 'ruby-openai', '~> 2.1'

  s.add_runtime_dependency 'tty-command', '~> 0.10.1'
  s.add_runtime_dependency 'tty-config', '~> 0.6.0'
  s.add_runtime_dependency 'tty-file', '~> 0.10.0'
  s.add_runtime_dependency 'tty-link', '~> 0.1.1'
  s.add_runtime_dependency 'tty-option', '~> 0.2.0'
  s.add_runtime_dependency 'tty-platform', '~> 0.3.0'
  s.add_runtime_dependency 'tty-prompt', '~> 0.23.1'
  s.add_runtime_dependency 'tty-spinner', '~> 0.9.3'
  s.add_runtime_dependency 'tty-which', '~> 0.5.0'

  # NOTE: Do not bump to 6 or greater.
  # See https://gitlab.com/huyderman/airy/-/issues/10
  s.add_runtime_dependency 'xdg', '~> 5.1', '< 6'

  s.add_development_dependency 'rubocop', '~> 1.39'
end
